# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Xian-He Sun
# <sun@iit.edu> 
#
# This file is part of HParser
# 
# HParser is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
"""
Safe
-> folder_pattern = r'(?:/[^/]+)+?'
-> file_pattern = r'(?:/[^/]+)+?/\w+\.\w+'

Perfect
-> all = r'((((?<!\w)[A-Z,a-z]:)|(\.{1,2}\\))([^\b%\/\|:\n\"]*))|("\2([^%\/\|:\n\"]*)")|((?<!\w)(\.{1,2})?(?<!\/)(\/((\\\b)|[^ \b%\|:\n\"\\\/])+)+\/?)'

"""

from prompt_toolkit import prompt
from prompt_toolkit.styles import Style
from prompt_toolkit.formatted_text import HTML, ANSI
import re
from art import *
import spacy
from nltk.tokenize import word_tokenize
import os
import subprocess
from defaults_config import offset_default,chunksize_default,buffer_default,stride_length_default
from termcolor import colored
from dicttoxml import dicttoxml
from xml.dom.minidom import parseString


#################################################### NLP Modules ####################################################

def replace_alpha(string):
    """
    """
    return re.sub("\D", "", string)
    # return "".join(c if not c.isalpha() else "" for c in string).strip()

def tokenize(text):
    """
    """
    # text = content.strip()
    tokens = word_tokenize(text)
    return tokenize


def extract_entities(text):
    """
    """
    doc = nlp(text)
    ner_ent = []
    for ent in doc.ents:
        ner_ent.append(ent.text)
    return ner_ent

def extract_path(text):
    """
    """
    path_regex = r'((((?<!\w)[A-Z,a-z]:)|(\.{1,2}\\))([^\b%\/\|:\n\"]*))|("\2([^%\/\|:\n\"]*)")|((?<!\w)(\.{1,2})?(?<!\/)(\/((\\\b)|[^ \b%\|:\n\"\\\/])+)+\/?)'
    path = max(re.findall(path_regex,text)[0],key=len)
    return path

def extract_info(values,ngrams,key):
    """
    Used to extract offset,chunksize, buffer tier
    postion of key has to be less than that of value
    """
    # key = "offset"
    for value in values:
        for g in ngrams:
            if key in g.lower() and str(value) in g.lower():
                if g.lower().index(key) < g.lower().index(str(value)):
                    return g

def extract_offset(values,ngrams):
    """
    """
    offset = extract_info(values,ngrams,key="offset")
    # print("offset_i : ",offset)
    if offset != None:
        offset = replace_alpha(offset).strip()
        return int(offset)
    else :
        return offset_default
        
def extract_chunksize(values,ngrams):
    """
    """
    chunksize = extract_info(values,ngrams,key="chunk")
    # print("chunksize_i : ",chunksize)
    if chunksize != None:
        chunksize = replace_alpha(chunksize).strip()
        return int(chunksize)
    else:
        return chunksize_default

def extract_buffer_tier(tokens):
    """
    """
    buffers = ["ram","nvme","ssd","hard disk"]
    b_r = None
    for b in buffers:
        for t in tokens:
            if b in t:
                b_r = b
    
    if b_r == None:
        return buffer_default
    
    return b_r

def extract_stride_length(values,ngrams):
    """
    """
    stride_length = extract_info(values,ngrams,key="stride")
    if stride_length != None:
        stride_length = replace_alpha(stride_length).strip()
        return int(stride_length)
    else:
        return stride_length_default

def extract_end(values,ngrams):
    """
    """
    end = extract_info(values,ngrams,key="end")
    if end  != None:
        end = replace_alpha(end).strip()
        return int(end)
    else:
        return end_default

def generate_ngrams(tokens, n):
    """
    """
    ngrams = zip(*[tokens[i:] for i in range(n)])
    return [" ".join(ngram) for ngram in ngrams]


#################################################### INFERENCE Modules ####################################################

def infer_file_type(path):
    """
    """
    out = subprocess.Popen(['file', path],
                            stdout=subprocess.PIPE, 
                            stderr=subprocess.STDOUT)

    file_type = out.split(":")[-1]
    return file_type


def infer_access_pattern(tokens,offset):
    """
    Use offset for 
    a) Sequential
    b) Random

    Check for stride info 
    c) Strided
    """

    if "stride" in tokens:
        return "Strided"

    if offset == 0 and ("stride" not in tokens):
        return "Sequential"
    
    if offset != 0 :
        return "Random"

def run_sequential(values,ngrams,path):
    """
    """
    offset = extract_offset(values=values,ngrams=ngrams)
    if not validate_offset(offset,filesize=file_size(file_path=path)):
        offset = None
    chunksize = extract_chunksize(values=values,ngrams=ngrams)
    if not validate_chunksize(offset=offset,chunk_size=chunksize,file_size=file_size(file_path=path)):
        chunksize = None
    buffer_tier = extract_buffer_tier(tokens=ngrams)
    r_dict = create_xml_dict(offset=offset,chunksize=chunksize,buffertier=buffer_tier,path=path)
    return r_dict

def run_random(values,ngrams,path):
    """
    """
    offset = extract_offset(values=values,ngrams=ngrams)
    if not validate_offset(offset,filesize=file_size(file_path=path)):
        offset = None
    chunksize = extract_chunksize(values=values,ngrams=ngrams)
    if not validate_chunksize(offset=offset,chunk_size=chunksize,file_size=file_size(file_path=path)):
        chunksize = None
    end = extract_end(values=values,ngrams=ngrams)
    if not validate_end(end=end,file_size=file_size(file_path=path)):
        end = None
    buffer_tier = extract_buffer_tier(tokens=ngrams)
    r_dict = create_xml_dict(offset=offset,chunksize=chunksize,end=end,buffertier=buffer_tier,path=path)
    return r_dict

def run_strided(values,ngrams,path):
    """
    """

    offset = extract_offset(values=values,ngrams=ngrams)
    if not validate_offset(offset,filesize=file_size(file_path=path)):
        offset = None
    stridelength = extract_stride_length(values=values,ngrams=ngrams)
    buffer_tier = extract_buffer_tier(tokens=ngrams)
    r_dict = create_xml_dict(offset=offset,stride=stridelength,buffertier=buffer_tier,path=path)
    return r_dict


#################################################### VALIDATION Modules ####################################################

def validate_path(path):
    """
    """
    verdict = os.path.isdir(path)
    if verdict:
        print(colored(("validating path: %s"%str(verdict)).upper(),color="yellow"))
        return "folder"
    elif not verdict:
        if os.path.exists(path):
            print(colored(("validating path: %s"%str(True)).upper(),color="yellow"))
            return "file"
        else:
            print(colored(("validating path: %s"%str(False)).upper(),color="yellow"))
            return "Not Valid"

def validate_offset(offset,filesize):
    """
    """
    if offset < filesize:
        print(colored(("validating offset: %s"%str(True)).upper(),color="yellow"))
        return True
    print(colored(("validating offset: %s"%str(False)).upper(),color="yellow"))
    return False

def validate_chunksize(offset,chunk_size,file_size):
    """
    """
    if offset+chunk_size <= file_size:
        print(colored(("validating chunksize: %s"%str(True)).upper(),color="yellow"))
        return True
    print(colored(("validating chunksize: %s"%str(False)).upper(),color="yellow"))
    return False

def validate_end(end,file_size):
    """
    """
    if end <= file_size:
        print(colored(("validating end: %s"%str(True)).upper(),color="yellow"))
        return True
    print(colored(("validating end: %s"%str(False)).upper(),color="yellow"))
    return False


#################################################### UTILITY Modules ####################################################

def file_size(file_path):
    """
    this function will return the file size
    """
    if os.path.isfile(file_path):
        file_info = os.stat(file_path)
        return file_info.st_size


def save_2_xml(results_dic,filename):
    """
    """
    root = "file"
    if type(results_dic) == list:
        root = "folder"
    print(colored("Extracted Entitites :",color='yellow'))
    # print(colored(str(results_dic),color='yellow'))
    xml = dicttoxml(results_dic, custom_root=root, attr_type=False)
    dom = parseString(xml)
    # print(colored(dom.toprettyxml(),color="yellow"))
    result = dom.toprettyxml()
    result = result.replace("item","file")
    print(colored(result,color="yellow"))
    with open("xml"+os.path.sep+filename+"_result.xml",'w') as wp:
        wp.write(result)
    pass

def create_xml_dict(path,offset=None,chunksize=None,end=None,stride=None,buffertier=None):
    """
    """
    results_dict = {}

    results_dict["path"] = path

    if offset == None:
        results_dict["offset"] = offset_default
    
    else:
        results_dict["offset"] = offset

    if chunksize == None:
        results_dict["chunksize"] = chunksize_default
    
    else:
        results_dict["chunksize"] = chunksize
    
    if end == None:
        results_dict["end"] = file_size(path)
    else:
        results_dict["end"] = end
    
    if stride == None:
        results_dict["stride"] = stride_length_default
    else:
        results_dict["stride"] = stride
    
    if buffertier == None:
        results_dict["buffer_tier"] = buffer_default
    else:
        results_dict["buffer_tier"] = buffertier

    return results_dict


def load_examples(filename):
    """
    """
    examples = []
    with open(filename,'r') as rp:
        for cnt,line in enumerate(rp):
            examples.append(line.strip())

    return examples

def extract_req(pattern,values,ngrams,path):
    """
    """
    if pattern == "Sequential":
        r_dict = run_sequential(values=values,ngrams=ngrams,path=path)
        save_2_xml(results_dic=r_dict,filename=path.split(os.path.sep)[-1])

    elif pattern == "Random":
        r_dict = run_random(values=values,ngrams=ngrams,path=path)
        save_2_xml(results_dic=r_dict,filename=path.split(os.path.sep)[-1])

    else:
        r_dict = run_strided(values=values,ngrams=ngrams,path=path)
        save_2_xml(results_dic=r_dict,filename=path.split(os.path.sep)[-1])


def run_file(text,path):
    """
    Extract the following:
    1) access pattern
    2) Based on access pattern extract the required information
    3) Sequential -> Offset, chunksize, buffer tier
    4) Random -> Offset,chunksize, end, buffer tier
    5) Strided -> Offset, stridelength, buffer tier
    """
    tokens = word_tokenize(text)
    tokens = [t.lower() for t in tokens]

    n_grams = generate_ngrams(tokens,n=4)

    ner_extracts = extract_entities(text)

    offset = extract_offset(values=ner_extracts,ngrams=n_grams)
    pattern = infer_access_pattern(tokens=tokens,offset=offset)
    print(colored(("Pattern Inferred : %s"%pattern),color='yellow'))
    extract_req(pattern=pattern,values=ner_extracts,ngrams=n_grams,path=path)
    pass

def run_folder(text,path):
    """
    For each file in folder,
    1) Get access pattern and run_file according to the pattern infered.
    """

    tokens = word_tokenize(text)
    tokens = [t.lower() for t in tokens]

    n_grams = generate_ngrams(tokens,n=4)

    ner_extracts = extract_entities(text)

    offset = extract_offset(values=ner_extracts,ngrams=n_grams)
    pattern = infer_access_pattern(tokens=tokens,offset=offset)
    print(colored(("Pattern Inferred : %s"%pattern),color='yellow'))
    files = os.listdir(path)
    dict_list = []
    if pattern == "Sequential":
        for f in files:
            full_path = path+os.path.sep+f
            r_dict = run_sequential(values=ner_extracts,ngrams=n_grams,path=full_path)
            dict_list.append(r_dict)
    elif pattern == "Random":
        for f in files:
            full_path = path+os.path.sep+f
            r_dict = run_random(values=ner_extracts,ngrams=n_grams,path=full_path)
            dict_list.append(r_dict)
    else:
        for f in files:
            full_path = path+os.path.sep+f
            r_dict = run_strided(values=ner_extracts,ngrams=n_grams,path=full_path)
            dict_list.append(r_dict)
    save_2_xml(results_dic=dict_list,filename=path.split(os.path.sep)[-1])


def print_usage(examples):
    """
    """
    print(colored("\n********************* USAGE INFORMATION *********************\n",color='yellow'))
    print(colored("You can just decribe the required buffering needs and this configuration manager will extract the required contents\n",color='yellow'))
    print(colored("\tFew Examples of how descriptions look are shown below :\n",color="yellow"))
    for i,e in enumerate(examples):
        print(colored(("%s) %s\n" %(str(i+1),e)),color='green'))
    
    print(colored("\tTo make the extractions more accurate pls use certain key words such as :\n",color='yellow'))
    print(colored("1) Offset - To indicate your starting position for buffering",color='green'))
    print(colored("2) end - To indicate where you want to stop (if you want to access only a part of the file)",color='green'))
    print(colored("3) chunksize - To indicate the what size of chunks are to be used when buffering large files",color='green'))
    print(colored("4) stride - To indicate the stride length if you want to employ a strided access patterns",color='green'))

    print(colored("\n\tThe Buffer Tiers that are available are as follows:",color='yellow'))
    print(colored("1) RAM",color='green'))
    print(colored("2) NVME",color='green'))
    print(colored("3) SSD",color='green'))
    print(colored("4) HARD DISK",color='green'))

    print(colored("\nThis module is still in development stages so you might not get the right results all the time, incase this fails to extract the intended information,".upper(),color="red"))
    print(colored("please use the direct commandline client for a more accurate experience \n".upper(),color='red'))

def prompt_custom():
    """
    """
    answer = prompt('> ', rprompt=' <HermesBufferSchemaManager> ', style=example_style)
    return answer

def welcome_message():
    """
    """

    tprint(text="HERMES", font="alpha")
    # print(colored("************************************************************************************************************************************************\n",color='yellow'))
    print(colored("###############################################################################################################################################\n",color='yellow'))
    print(colored("\t\t\t\t\tThis is the User Defined Buffering Management tool for HERMES".upper(),color='blue'))
    print("\n")
    print(colored("\t\t\t\tThis manager enables you to define user buffering schemas for the Hermes I/O Platform".upper(),color='blue'))
    print(colored("###############################################################################################################################################\n",color='yellow'))
    # print(colored("************************************************************************************************************************************************\n",color='yellow'))

if __name__ == "__main__":

    # Load NLP Model to extract data using NER
    nlp = spacy.load("en_core_web_sm")
    # Prompt Style
    example_style = Style.from_dict({'rprompt': 'bg:#077c9f #ffffff',})
    
    # Print Welcome Message()
    welcome_message()

    examples = load_examples(filename="xml/examples.txt")
    print_usage(examples)
    print(colored("Type Q to quit Anytime".upper(),color='blue'))
    while True:
        content = prompt_custom()

        text = content.strip()
        if text == "Q" or text=="q":
            break
        # Check if file or folder
        path = extract_path(text=text)
        ver_p = validate_path(path)
        if ver_p == "file":
            run_file(text=text.lower(),path=path)
        if ver_p == "folder":
            run_folder(text=text.lower(),path=path)
        if ver_p == "Not Valid":
            print("\nSorry the given file/folder does not exist !! Please check the path and start again\n")
            break

        

# 1) Ner Model 
# 2) If it fails,
# 3) retrain, logistic regression
# 4) For every key , what is the best n value.
# 5) check word similarity, for the key words.
# 6) for each key we have a n value.
# 7) cluster represents a key.
