# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Xian-He Sun
# <sun@iit.edu> 
#
# This file is part of HParser
# 
# HParser is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
offset_default = 0
chunksize_default = 100
buffer_default = "ram"
stride_length_default = 0
