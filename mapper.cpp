/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Xian-He Sun
 * <sun@iit.edu> 
 *
 * This file is part of HParser
 * 
 * HParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <string>
#include <iostream>
#include <unordered_map>
# include <vector>

namespace pt = boost::property_tree;

struct Schema
{
    std::string path ;
    std::string type ;
    std::string access_pattern ;
    std::string offset;
    std::string chunk_size;
    std::string tier;

// Override boolean operator == for hash collisions
  bool operator==(const Schema &other) const{
      return (path == other.path 
      && type == other.type 
      && access_pattern == other.access_pattern 
      && offset == other.offset 
      && chunk_size == other.chunk_size 
      && tier == other.tier );
  }
};


// override the hash function for our struct, so it can be used in unorderd map
namespace std {

    template <>
    struct hash<Schema>
    {
        std::size_t operator()(const Schema& k) const
        {
            using std::size_t;
            using std::hash;
            using std::string;

            return (hash<string>()(k.type+k.path+k.access_pattern+k.chunk_size+k.offset+k.tier));
        }
    };
}

void print_map(std::unordered_map<Schema,std::string> map){

    for (auto& x: map){

        std::cout << "\n******************************************\n" ;
        std::cout << "\nKey :\n\n" ;
        std::cout << "type = " << x.first.type << "\n" ;
        std::cout << "path = " << x.first.path << "\n" ;
        std::cout << "access_pattern = " << x.first.access_pattern << "\n" ;
        std::cout << "offset = " << x.first.offset << "\n" ;
        std::cout << "chunk_size = " << x.first.chunk_size << "\n" ;
        std::cout << "tier = " << x.first.tier << "\n" ; 

        std::cout << "\nValue :\n\n" ;
        std::cout << x.second <<"\n" ;
        std::cout << "\n******************************************\n" ;

    }
}


Schema getmap(pt::ptree::value_type v){

    Schema sch ;

    BOOST_FOREACH(pt::ptree::value_type &w,v.second){
                if (w.first == "type"){
                    sch.type = w.second.data() ;
                }
                else if(w.first == "path"){
                    sch.path = w.second.data();
                }
                else if (w.first == "access_pattern"){
                    sch.access_pattern = w.second.data();
                }
                else if (w.first == "offset"){
                    sch.offset = w.second.data();
                }
                else if (w.first == "chunk_size"){
                    sch.chunk_size = w.second.data() ;
                }
                else if (w.first == "tier"){
                    sch.tier = w.second.data() ;
                }
            }    

    return sch ; 
}


int main() {
    
    // Create empty property tree object
    pt::ptree tree;

    Schema* schemas = new Schema[10]; 

    // std::vector<Schema> schearr;

    std::unordered_map<Schema,std::string> map_schema;

    // Parse the XML into the property tree.
    pt::read_xml("xml/results.xml", tree);

    BOOST_FOREACH(pt::ptree::value_type &v,tree.get_child("root")){

        Schema sch = getmap(v);

        // schearr.push_back(sch);

        map_schema.insert({sch,sch.tier});
    }

    print_map(map_schema) ;

    return 0 ;
}
