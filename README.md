# README
The main file to run is : **cmd_client.py**

The xml folder contains the schema for the questions and stores the intermediate xml files.

The C++ mapper file (**mapper.cpp**) just converts the xml and creates an unordered dictionary.

If you run the **mapper.cpp** file it will print the dictionary.
The rest are configuration and utility files.
