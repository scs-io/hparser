# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Xian-He Sun
# <sun@iit.edu> 
#
# This file is part of HParser
# 
# HParser is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
"""Attempt at objects for intermediate storage of xml parsing results"""
class hFolder:
    def __init__(self, path = "", files = {}, tier = -1, externalServices = -1):
        self.path = path
        self.files = files
        self.tier = tier
        self.e_s = externalServices
        #Not sure of number of tiers
        
    def __granularityCheck__(self):
        assert 0 < self.tier < 4 or self.__checkGranularityFile__()
        
    def __checkGranularityFile__(self):
        return all(self.files.tier > 0) or all(self.files.__checkGranularityChunk__())        
        
    def __setattr__(self, attr, value):
        super().__setattr__(attr, value)
        
    def __getattr__(self, attr, value):
        super().__getattr__(attr, value)
        #DS for storing files
        #Triggers: don't look at granularity (or something like that)
        
class hFile:
    def __init__(self, name = "", path = "", chunks = [], tier = -1, externalServices = -1): 
        self.name = name
        self.path = path
        self.chunks = chunks #Should be a dict/list of Chunks 
        self.tier = tier     #Chunks can be identified by their offset
        self.e_s = externalServices 
    
    def __checkGranularityChunk__(self):
        return all(self.chunks.tier > 0)

    def __setattr__(self, attr, value):
        super().__setattr__(attr, value)
        
    def __getattr__(self, attr, value):
        super().__getattr__(attr, value)
        #DS for storing chunks
        #Vars(obj) prints a dictionary where each key 
        #Where to build the logic to check offset + size conflicts?
        
class hChunk:
    def __init__(self, offset, size, tier = -1):
        self.offset = offset
        self.size = size
        self.tier = tier
    
    def __setattr__(self, attr, value):
        super().__setattr__(attr, value)
        
    def __getattr__(self, attr, value):
        super().__getattr__(attr, value)
