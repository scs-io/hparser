Buffer a file located at /home/scraper/Schema_Analyzer/Code/test/test_file with an offset of 0 bytes and chunk size of 100 bytes into RAM.
Buffer a file located at /home/scraper/Schema_Analyzer/Code/test/test_file with an offset of 100 bytes and chunk size of 50 bytes, end at 1000 bytes and buffer it in Hard disk.
Buffer a file located at /home/scraper/Schema_Analyzer/Code/test/test_file with an offset of 0 bytes and stride length of 10 bytes into NVME.
Buffer a folder located at /home/scraper/Schema_Analyzer/Code/test/test_fol with an offset of 0 bytes and chunk size of 100 bytes into RAM.