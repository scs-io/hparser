# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Xian-He Sun
# <sun@iit.edu> 
#
# This file is part of HParser
# 
# HParser is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
"""
How the client works:
a) Read the XML - Get the topmost level question / Node
b) print the question and options and get the answer
c) Based on the answer provided move on to other nodes

Validation:
It is done when the user enters the answer, then the validate attribute for the xml node is used
to check if the format is in the right manner and also for choosing a path down our tree

-> Finally all the answers are recorded and are populated as a new attribute to each node called "ans"
-> and saved in a new xml file, After which it is picked up by the converter which converts the xml into 
-> a more compact format for the mapper (C++ module) and the mapper converts this file to an unordered map
-> and sends it to Hermes.

So what do we need ?
-> Main Loop to iterate over questions
-> Method to print,validate and record answers
-> A method to generate a new XML from the recorded answers

"""

from __future__ import unicode_literals
from prompt_toolkit import prompt
from prompt_toolkit.styles import Style
from prompt_toolkit.formatted_text import HTML, ANSI
from collections import defaultdict
import xml.etree.ElementTree as ET
from config import result_path,results_name,questions_path,questions_name
import os
from art import *


######################################## Utilitiy Methods ########################################
def prompt_custom():
    """
    A function that just generates a Custom Prompt for this cmd client
    """
    answer = prompt('> ', rprompt=' <HermesBM> ', style=schbstyle)
    return answer


def welcome_message():
    """
    Prints a welcome message and explains what the cmd line client does
    """

    tprint(text="HERMES", font="alpha")
    print("************************************************************************************************************************************************\n")
    print("\t\t\t\t\tThis is the User Defined Buffering Management tool for HERMES")
    print("\n")
    print("\t\tThis manager enables you to store user defined configuration for buffering schemes for the Hermes Platform")
    print("************************************************************************************************************************************************\n")


######################################## Node Methods ########################################

def required_node(node,answer):
    """
    It is a function that helps select the next node in our tree
    based on the previous answer
    """

    temp = node.attrib["validate"]
    if len(temp)==0:
        return True
    else:
        if temp.lower() == answer.lower():
            return True
        elif (temp.lower() in  answer.lower()) or (answer.lower() in temp.lower()) :
            return True
        else:
            return False

def run_node(node):
    """
    This function runs a given question from xml, validates it and stores the answer
    defined by the user.
    """

    global answers
    global run_count

    print("\n")
    print(node.attrib["text"])
    print("\nOptions:")
    print(node.attrib["options"]+"\n")
    format_a = node.attrib["req"]
    if "," in format_a:
        format_a = format_a.split(",")
    ans = prompt_custom()
    

    if type(format_a) is list:
        while True:
                is_t = False
                for k in format_a:
                    if k.lower() in ans.lower():
                        is_t = True
                        break
                if is_t == False:
                    print("I'm Sorry the Answer you have entered is not in the Right Format !!")
                    print("Please Enter your Answer in the Right Format or Type 'Q' to Quit")
                    ans = prompt_custom()
                if is_t == True:
                    break
                if ans.strip() == "Q" or ans.strip() == "q":
                    break

    else:
        while True:

            if format_a.lower() not in ans.lower():
                print("I'm Sorry the Answer you have entered is not in the Right Format !!")
                print("Please Enter your Answer in the Right Format or Type 'Q' to Quit")
                ans = prompt_custom()

            if ans.strip() == "Q" or ans.strip() == "q":
                break

            if format_a.lower() in ans.lower():
                break

    node.set('ans',ans)
    answers[run_count].append((node.attrib["id"], ans ))
    return ans
 

######################################## Result Tree Methods ########################################

def create_result_root():
    """
    Create a xml tree to record all the answers with the attribute type
    """
    root = ET.Element("root")
    return root


def create_tree(root,attrib_list):
    """
    """
    child = ET.SubElement(root,"file")
    for attrib in attrib_list:
        if ":" in attrib[1]:
            value = attrib[1].split(":")
            ET.SubElement(child,attrib[0]).text = value[1]    
        else:
            ET.SubElement(child,attrib[0]).text = attrib[1]

if __name__ == "__main__":

    answers=defaultdict(list)

    result_xml = create_result_root()

    # used for multiple File Buffering
    run_count = 1

    # Set the style for the prompt
    schbstyle = Style.from_dict({'rprompt': 'bg:#077c9f #ffffff',})

    #Print Welcome Message
    welcome_message()

    while True:

        # Parse the XML file to get a tree structure
        tree = ET.parse(questions_path+os.path.sep+questions_name)
        root = tree.getroot()

        # Get the start node
        start_node = [child for child in root][0]

        # Main Loop to walk down the questionaire
        while True:
            # Get the answer
            answer = run_node(start_node)
            
            # If node has no more children, reached the end 
            if len(list(start_node)) <= 0:
                break

            if answer == "Q" or answer == "q":
                break
            
            # Check which node is next based on the previous answer
            if len(list(start_node)) > 0:
                for child in start_node:
                    needed = required_node(child,answer)
                    if needed:
                        start_node = child
                        break


        print("\nDo you want to Buffer More Files ? (Y(yes)/N(no))")
        ans = prompt_custom()
        if ans.strip()== "N" or ans.strip()== "n":
            break

        run_count+=1 


    # Now store all the answers to a new file:
    for run in answers:
        create_tree(root=result_xml,attrib_list=answers[run])
    
    tree = ET.ElementTree(result_xml)
    tree.write(result_path+os.path.sep+results_name+".xml")
