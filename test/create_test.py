# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Xian-He Sun
# <sun@iit.edu> 
#
# This file is part of HParser
# 
# HParser is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

import os

def create(ind=None,path=None):
    """
    """
    file_name = None
    if ind == None:
        file_name = 'test_file'
    else:
        file_name = path+os.path.sep+'test_file'+"_"+str(ind)
    file_size = 1024*100 # size in bytes
    with open(file_name, "wb") as f:
        f.write(os.urandom(file_size))

def create_fol():
    os.makedirs("test_fol")
    for i in range(10):
        create(ind=i,path="test_fol")

if __name__ == "__main__":
    create()
    create_fol()
